package zombieversity.controller;

/**
 * Interface to handle rounds flow.
 *
 */
public interface RoundController {

    /**
     * Used to update round model and view.
     */
    void update();

}
